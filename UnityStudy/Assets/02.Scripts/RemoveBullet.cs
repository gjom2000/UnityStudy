using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveBullet : MonoBehaviour
{
    public GameObject sparkEffect;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("BULLET"))
        {
            // 첫 번째 충돌 지점의 정보 추출
            ContactPoint cp = collision.GetContact(0);

            // 충돌한 총알으 ㅣ법선 벡터를 쿼터니언 타입으로 변환
            Quaternion rot = Quaternion.LookRotation(-cp.normal);

            // 스파크 파이클을 동적으로 생성
            GameObject spark = Instantiate(sparkEffect, cp.point, rot);

            // 일정 시간이 지난 후 파이클 제거
            Destroy(spark, 0.5f);

            Destroy(collision.gameObject);
        }
    }
}
